const express = require('express');
const fs = require('fs');

const path = "./messages";

const router = express.Router();

let messagesDate = [];
let messages = [];

router.get('/', (req, res) => {
    fs.readdir(path, (err, files) => {
      files.forEach(file => {
        const fileName = path + '/' + file;
        messagesDate.push(fileName);
      })

      const lastFiveMessages = messagesDate.slice(-5);

      lastFiveMessages.forEach(msg => {
        fs.readFile(msg, (err, data) => {
          if (err) {
            console.error(err);
          } else {
            messages.push(JSON.parse(data));
          }
        })
      });

      let arr = messages;
      messages = [];

      return res.send(arr);
    });
});

  router.post('/', (req, res) => {

    const currentDate = new Date().toISOString();
    const fileName = `./messages/${currentDate}.txt`;

    const message = {
      message: req.body.message,
      datetime: currentDate,
    };

    fs.writeFile(fileName, JSON.stringify(message), (err) => {
      if (err) {
        console.error(err);
      } else {
        messages.push(message);
      }
    });

    return res.send('Created new message ' + message.datetime);
  });


module.exports = router;